from django.db import models

# Create your models here.

class role(models.Model):
    name = models.CharField(max_length=50)
    rights = models.IntegerField() 

    def get_model_type(self):
        return "role"

class teacher(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    position = models.ForeignKey(role)

    def get_model_type(self):
        return "teacher"


class group(models.Model):
    id = models.CharField(max_length=10, primary_key=True)

    def get_model_type(self):
        return "group"


class student(models.Model):
    name = models.CharField(max_length=50)
    surname = models.CharField(max_length=50)
    position = models.ForeignKey(role)
    group = models.ForeignKey(group)

    def get_model_type(self):
        return "student"

  
class subject(models.Model):
    name = models.CharField(max_length=50)

    def get_model_type(self):
        return "subject"

 
class student_has_subject(models.Model):
    student = models.ForeignKey(student)
    subject = models.ForeignKey(subject)
    mark = models.CharField(max_length=100)

    def get_model_type(self):
        return "student_has_subject"

 
class teacher_has_group(models.Model):
    teacher = models.ForeignKey(teacher)
    group = models.ForeignKey(group)

    def get_model_type(self):
        return "teacher_has_group"


