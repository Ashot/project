from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import HttpResponse
from itertools import chain
from django.core.context_processors import csrf

from .models import student
from .models import teacher
from .models import group
from .models import role
from .models import subject
from .models import teacher_has_group
from .models import student_has_subject


def school(request):
    c = {}
    c.update(csrf(request))
    return render_to_response("e_school/school.html", c)

def get_tutors_list(request):
    tutors_list = teacher.objects.all()	
    if tutors_list:
        res = [1, tutors_list]
    else:
        res = [0, "No teachers"]
    return render(request, 'e_school/get_tutors_list.html', {'teachers' : res})

def get_pupils_list(request):
    pupils_list = student.objects.filter(group_id=request.GET['q'])
    if pupils_list:
        res = [1, pupils_list]
    else:
        res = [0, "No pupils"]   
    return render(request, 'e_school/get_pupils_list.html', {'pupils' : res})


def get_pupil_disciplines(request):
    subjects_list = subject.objects.filter(student_has_subject__student_id=request.GET['q'])   
    return render(request, 'e_school/get_pupil_disciplines.html', {'subjects_count' : len(subjects_list)})


def get_pupil_stat_list(request):
    subjects_list = subject.objects.filter(student_has_subject__student_id=request.GET['q'])   
    mark = student_has_subject.objects.filter(student_id=request.GET['q'])
    tmp = [j for i in zip(subjects_list,mark) for j in i]
    if tmp:
        res = [1, tmp]
    else:
        res = [0, "Stats empty"]
    return render(request, 'e_school/get_pupil_stat_list.html', {'data' : res})

def get_stat(s):
    students = student.objects.filter(group_id=s)
    subjects_list = subject.objects.all()  
    shs = student_has_subject.objects.filter(student_id__in=students)

    res = []
    for s in students.iterator():
        res.append(s)
        for tmp in shs.iterator():
            if (s.id != tmp.student_id):
                continue
            else:
                res.append(subjects_list[tmp.subject_id - 1])
                res.append(tmp)
    return res


def get_class_stat(request):
    role_cur = role.objects.filter(teacher__id=request.GET['p'])
    role_dir = role.objects.filter(id='3')
    role_teacher = role.objects.filter(id='2')
    res = []

    if (role_cur and role_dir and role_cur[0].rights == role_dir[0].rights):
        stat = get_stat(request.GET['q'])
        res = [1, stat] 

    elif (role_cur and role_teacher and role_cur[0].rights == role_teacher[0].rights):
        group_list = teacher_has_group.objects.filter(group_id=request.GET['q'], teacher_id=request.GET['p'])
        if (group_list):
            stat = get_stat(request.GET['q'])
            res = [1, stat]
        else:
            res = [0, "You have no rights"] 
    
    if (not res or not res[1]):  
        res = [0, "Wrong id"]
    return render(request, 'e_school/get_class_stat.html', {'data' : res})


def set_pupil_point(request):
    st_group = group.objects.filter(student__id = request.POST['q']).first()
    if st_group:
        thg  = teacher_has_group.objects.filter(group_id=st_group.id, teacher_id=request.POST['p'])
        if thg:
            sub = subject.objects.filter(name=request.POST['d']).first()
   	    if sub:
	        s = student_has_subject.objects.filter(student_id = request.POST['q'], subject_id = sub.id).first()
	        if s:
	            s.mark += request.POST['s']
                    s.save()
	            res = [1, "Complete"]

	        else:
		    res = [0, "Student does not have that subject"]
	    else:
	        res = [0, "Subject does not exist"]
        else:
            res = [0, "Teacher have no that group"]
    else:
        res = [0, "Student does not exist"]
    
    return render(request, 'e_school/set_pupil_point.html', {'data' : res})


