from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'set_pupil_point/$', views.set_pupil_point, name='set_pupil_point'),
    url(r'get_class_stat/$', views.get_class_stat, name='get_class_stat'),
    url(r'get_pupil_stat_list/$', views.get_pupil_stat_list, name='get_pupil_stat_list'),
    url(r'get_pupil_disciplines/$', views.get_pupil_disciplines, name='get_pupil_disciplines'),
    url(r'get_pupils_list/$', views.get_pupils_list, name='get_pupils_list'),
    url(r'^get_tutors_list/$', views.get_tutors_list, name='get_tutors_list'),
    url(r'^school/$', views.school, name='school'),
   
]
