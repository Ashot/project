from django.test import TestCase
from django.core.management import call_command
from django.test import Client


from .models import student
from .models import teacher
from .models import group
from .models import role
from .models import subject
from .models import teacher_has_group
from .models import student_has_subject
 


class MyTestCase(TestCase):
    def forwards(self, orm):

        call_command('loaddata', 'initial_data.json')

    def test1(self):
        c = Client()
        response = c.get('/get_tutors_list/')
        self.assertEqual(response.status_code, 200)

    def test2(self):
        c = Client()
        response = c.get('/get_tutors_list/')
        self.assertEqual(response.content.find('No teachers'), -1)

    def test3(self):
        c = Client()
        response = c.get('/get_pupils_list/', {'q': '1'})
        self.assertEqual(response.status_code, 200) 

    def test4(self):
        c = Client()
        response = c.get('/get_pupils_list/', {'q':'6'})
        self.assertNotEqual(response.content.find('No pupils'), -1)   

    def test5(self):
        c = Client()
        response = c.get('/get_pupils_list/', {'q':'1'})
        self.assertEqual(response.content.find('No pupils'), -1)  

    def test6(self):
        c = Client()
        response = c.get('/get_pupil_disciplines/', {'q':'1'})
        self.assertEqual(response.status_code, 200)

    def test7(self):
        c = Client()
        response = c.get('/get_pupil_disciplines/', {'q':'1'})
        self.assertEqual(response.content.find('0'), -1)  

    def test8(self):
        c = Client()
        response = c.get('/get_pupil_disciplines/', {'q':'11'})
        self.assertNotEqual(response.content.find('0'), -1)

    def test9(self):
        c = Client()
        response = c.get('/get_pupil_stat_list/', {'q':'1'})
        self.assertEqual(response.status_code, 200)


    def test10(self):
        c = Client()
        response = c.get('/get_pupil_stat_list/', {'q':'1'})
        self.assertEqual(response.content.find('Stats empty'), -1)

    def test11(self):
        c = Client()
        response = c.get('/get_pupil_stat_list/', {'q':'11'})
        self.assertNotEqual(response.content.find('Stats empty'), -1)

    def test12(self):
        c = Client()
        response = c.get('/get_class_stat/', {'q':'1', 'p':'1'})
        self.assertEqual(response.status_code, 200)

    def test13(self):
        c = Client()
        response = c.get('/get_class_stat/', {'q':'1', 'p':'1'})
        self.assertEqual(response.content.find('Wrong id'), -1)

    def test14(self):
        c = Client()
        response = c.get('/get_class_stat/', {'q':'10', 'p':'1'})
        self.assertNotEqual(response.content.find('Wrong id'), -1)

    def test15(self):
        c = Client()
        response = c.get('/get_class_stat/', {'q':'1', 'p':'10'})
        self.assertNotEqual(response.content.find('Wrong id'), -1)


    def test16(self):
        c = Client()
        response = c.get('/get_class_stat/', {'q':'1', 'p':'1'})
        self.assertEqual(response.content.find('You have no rights'), -1)

    def test17(self):
        c = Client()
        response = c.get('/get_class_stat/', {'q':'1', 'p':'2'})
        self.assertNotEqual(response.content.find('You have no rights'), -1)

    def test18(self):
        c = Client()
        response = c.post('/set_pupil_point/', {'d':'math', 's':'2', 'q':'1', 'p':'1'})
        self.assertEqual(response.status_code, 200)

    def test19(self):
        c = Client()
        response = c.post('/set_pupil_point/', {'d':'math', 's':'2', 'q':'1', 'p':'1'})
        self.assertNotEqual(response.content.find('Complete'), -1)

    def test20(self):
        c = Client()
        response = c.post('/set_pupil_point/', {'d':'1', 's':'2', 'q':'1', 'p':'1'})
        self.assertNotEqual(response.content.find('Subject does not exist'), -1)

    def test21(self):
        c = Client()
        response = c.post('/set_pupil_point/', {'d':'math', 's':'2', 'q':'15', 'p':'1'})
        self.assertNotEqual(response.content.find('Student does not exist'), -1)

    def test22(self):
        c = Client()
        response = c.post('/set_pupil_point/', {'d':'math', 's':'5', 'q':'1', 'p':'2'})
        self.assertNotEqual(response.content.find('Teacher have no that group'), -1)
