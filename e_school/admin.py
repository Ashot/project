from django.contrib import admin

# Register your models here.

from django.contrib import admin
from .models import teacher
from .models import role
from .models import student
from .models import subject
from .models import group
from .models import student_has_subject
from .models import teacher_has_group



admin.site.register(teacher)
admin.site.register(student)
admin.site.register(role)
admin.site.register(subject)
admin.site.register(group)
admin.site.register(student_has_subject)
admin.site.register(teacher_has_group)
