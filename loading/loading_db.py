import csv
import MySQLdb

connection = MySQLdb.connect('localhost', 'root', '12301230a', db='test')
							 
student_data = csv.reader(file('student.csv'))
teacher_data = csv.reader(file('teacher.csv'))
group_data = csv.reader(file('group.csv'))
subject_data = csv.reader(file('subject.csv'))
role_data = csv.reader(file('role.csv'))
ths_data = csv.reader(file('teacher_has_group.csv'))
shs_data = csv.reader(file('student_has_subject.csv'))

cursor = connection.cursor()

cursor.execute("SET FOREIGN_KEY_CHECKS=0")
cursor.execute("TRUNCATE TABLE e_school_student")
for row in student_data:
	cursor.execute("insert into e_school_student(id, name, surname, group_id, \
		position_id) values(%s, %s, %s, %s, %s)", row)
connection.commit()
print "Student done"

cursor.execute("TRUNCATE TABLE e_school_teacher")
for row in teacher_data:
	cursor.execute("insert into e_school_teacher(id, name, surname, \
		position_id) values(%s, %s, %s, %s)", row)
connection.commit()
print "Teacher done"

cursor.execute("TRUNCATE TABLE e_school_group")
for row in group_data:
	cursor.execute("insert into e_school_group(id) values(%s)", row)
connection.commit()
print "group done"

cursor.execute("TRUNCATE TABLE e_school_subject")
for row in subject_data:
	cursor.execute("insert into e_school_subject(id, name) values(%s, %s)", row)
connection.commit()
print "Subject done"

cursor.execute("TRUNCATE TABLE e_school_role")
for row in role_data:
	cursor.execute("insert into e_school_role(id, name, rights) values(%s, %s, %s)", row)
connection.commit()
print("Role done")

cursor.execute("TRUNCATE TABLE e_school_student_has_subject")
for row in shs_data:
	cursor.execute("insert into e_school_student_has_subject(id, student_id, subject_id, mark) \
		values(%s, %s, %s, %s)", row)
connection.commit()
print "Student has subject done"

cursor.execute("TRUNCATE TABLE e_school_teacher_has_group")
for row in ths_data:
	cursor.execute("insert into e_school_teacher_has_group(id, teacher_id, \
		group_id) values(%s, %s, %s)", row)
connection.commit()
print "Teacher has subject done"

cursor.close()
